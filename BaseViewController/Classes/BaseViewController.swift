//
//  XIBView.swift
//  Kinder Learn
//
//  Created by Madhup Yadav on 18/08/19.
//  Copyright © 2019 Kinder Learn. All rights reserved.
//

import UIKit
import PureLayout
let PAD = UIDevice.current.userInterfaceIdiom == .pad
let PHONE = UIDevice.current.userInterfaceIdiom == .phone

class BaseViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var tableVw:UITableView?
    @IBOutlet var collectionView:UICollectionView?

    let userDefaults:UserDefaults = UserDefaults.standard
    var data:Any?
    var bgImageView:UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bgImageView = UIImageView.init(frame: self.view.bounds)
        bgImageView?.image = UIImage.init(named: "BG")
        bgImageView?.contentMode = .scaleAspectFill
        bgImageView?.clipsToBounds = true
        self.view.addSubview(bgImageView!)
        self.view.sendSubviewToBack(bgImageView!)
        bgImageView?.autoPinEdgesToSuperviewEdges()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateCollectionViewLayout()
    }
    
    func changeBackgroundImage(_ image: UIImage?){
        bgImageView?.image = image
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    func updateCollectionViewLayout(){
        if self.collectionView == nil{
            return
        }
        let screen_width = UIScreen.main.bounds.size.width
        //        let screen_height = UIScreen.main.bounds.size.height
        var width:CGFloat = 0.0
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        if UIDevice.current.orientation.isLandscape{
            width = floor(screen_width * (PHONE ? 0.37 : 0.22))
            
        }else{
            width = floor(screen_width * (PHONE ? 0.75 : 0.27))
            
        }
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = CGSize(width: width, height: width)
        
        if UIDevice.current.orientation.isLandscape{
            let numberOfCells = CGFloat((PHONE ? 2.0 : 4.0))
            width = floor((screen_width - (width * numberOfCells))/(numberOfCells + 1))
        }else{
            let numberOfCells = CGFloat((PHONE ? 1.0 : 3.0))
            width = floor((screen_width - (width * numberOfCells))/(numberOfCells + 1))
        }
        UIDevice.current.endGeneratingDeviceOrientationNotifications()
        layout.sectionInset = UIEdgeInsets(top: width, left: 10, bottom: width, right: 10)
        //        layout.minimumLineSpacing = width
        //        layout.minimumInteritemSpacing = width
        collectionView?.setCollectionViewLayout(layout, animated: true)
    }
    
}



//MARK:- Collection View Defaults

extension BaseViewController{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for:  indexPath)
        return cell
    }
}



//MARK:- Table View Defaults

extension BaseViewController{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
        return cell!
    }
}
